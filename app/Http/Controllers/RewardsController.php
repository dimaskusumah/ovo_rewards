<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
/**
 *  Rewards
 */
class RewardsController extends Controller
{

    function __construct()
    {
        // code...
    }

    public function getRewards(Request $req, $name)
    {

        $todayRewards = \DB::select("SELECT * FROM daily_rewards WHERE date_rewards = '". date('Y-m-d') ."'");

        try {

            $results = \DB::select("SELECT * FROM users WHERE name like '%" . $name . "%'");

        } catch (\Exception $e) {

            return response()->json(['error' => $e], 500, ['X-Header-One' => 'Header Value']);

        }

        $getRewards = rand($results[0]->reward_range_from, $results[0]->reward_range_to);

        $leftRewards = $todayRewards[0]->today_rewards - $getRewards;

        \DB::select("INSERT INTO daily_user_rewards (user_id, reward_amount, reward_date) VALUES ('". $results[0]->users_id ."', '". $getRewards ."', '". date('Y-m-d') ."')");

        \DB::select("UPDATE daily_rewards SET today_rewards = $leftRewards WHERE date_rewards = '". date('Y-m-d') ."'");

        $message = array(
            'Name' => $name,
            'Point Rewards' => $getRewards,
            'Rewards Left for Today' => $leftRewards
        );

        return response()->json($message);

    }

}
